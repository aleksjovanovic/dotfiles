# Dependencies
1. Google-Chrome
2. xclip
3. oh-my-zsh
4. openssh
5. pass
6. zip & unzip
7. libusb-compat & ccid
8. pcsclite & pcsc-tools
9. python & pip
10. awscli

## Install Command
```
yaourt google-chrome
sudo pacman -S xclip
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sudo pacman -S openssh
sudo pacman -S pass
sudo pacman -S zip unzip
sudo pacman -S libusb-compat ccid
sudo pacman -S pcsclite pcsc-tools
sudo pacman -S python python2-pip
pip2 install awscli --upgrade --user

```

## Enable Yubikey service & socket
```
systemctl enable pcscd.socket
systemctl enable pcscd.service

```
