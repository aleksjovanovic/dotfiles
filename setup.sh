#!/bin/bash

echo "Do you want to install system packages? (Y/n)" 
read installPackages
if [ "$installPackages" == "Y" ] || [ "$installPackages" == "y" ]; then
  sudo pacman --noconfirm -S - < ~/.dotfiles/packages/pacman.txt
fi

echo "Do you want to install aur packages? (Y/n)" 
read installAurPackages
if [ "$installAurPackages" == "Y" ] || [ "$installAurPackages" == "y" ]; then
  #sudo pacman -S < ~/.dotfiles/packages/pacman.txt
  echo "TODO!"
fi

echo "Do you want to install python packages? (Y/n)" 
read installPythonPackages
if [ "$installPythonPackages" == "Y" ] || [ "$installPythonPackages" == "y" ]; then
  pip2 install --upgrade --user -r ~/.dotfiles/packages/python2.txt
fi

echo "Do you want to install ZSH? (Y/n)"
read installZsh
if [ "$installZsh" == "Y" ] || [ "$installZsh" == "y" ]; then
  echo "Downloading oh-my-zsh..."
  cd ~/
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
fi

echo "Do you want to install zsh-plugins? (Y/n)"
read installZshPlugins
if [ "$installZshPlugins" == "Y" ] || [ "$installZshPlugins" == "y" ]; then
  echo "Installing plugins..."
  mkdir -p ~/.dotfiles/zsh/plugins
  cd ~/.dotfiles/zsh/plugins
  git clone https://github.com/zsh-users/zsh-autosuggestions.git
fi

echo "Linking vimrc..."
ln -sf ~/.dotfiles/vim/vimrc ~/.vimrc

echo "Linking i3..."
mkdir -p ~/.i3
ln -sf ~/.dotfiles/i3/config ~/.i3/config

echo "Linking  zsh..."
ln -sf ~/.dotfiles/zsh/zshenv ~/.zshenv 
ln -sf ~/.dotfiles/zsh/zshrc ~/.zshrc 

echo "Linking compton..."
ln -sf ~/.dotfiles/compton/compton.conf.old ~/.config/compton.conf

echo "Linking Urxvt (Xresources)..."
ln -sf ~/.dotfiles/x/Xresources ~/.Xresources

echo ¨Setup default keyboard layout...¨
sudo cp ~/.dotfiles/misc/keyboard /etc/default/keyboard

echo "Restart all processes..."
i3-msg restart
#pkill compton
#exec compton

echo "Restart services for current session..."
sudo systemctl restart sshd.service
sudo systemctl restart pcscd.service

echo "Enable services for future use..."
sudo systemctl enable sshd.service
sudo systemctl enable pcscd.service

echo "Sync system time..."
sudo ntpdate pool.ntp.org

echo "All Done!"
