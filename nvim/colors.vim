" Themes, colors, syntax related config

" Status Line Config
set laststatus=2
set noshowmode
let g:lightline = {
  \ 'colorscheme': 'gruvbox',
  \ }

" Colorscheme
colorscheme gruvbox
set background=dark

