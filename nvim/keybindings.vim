" Short Keys
" map <C-o> :NERDTreeToggle<CR>

" Leader
let mapleader = ";"

" Custom Commands
nnoremap <leader>jp :JsonPretty<CR>
nnoremap <leader>jm :JsonMinify<CR>
nnoremap <leader>sh :split<CR>
nnoremap <leader>sv :vsplit<CR>
" nnoremap <leader>cp :CopySelection<CR>

" Navigation between windows since MacOS is borderline retarded
nnoremap <leader>wh :wincmd h<CR>
nnoremap <leader>wj :wincmd j<CR>
nnoremap <leader>wk :wincmd k<CR>
nnoremap <leader>wl :wincmd l<CR>

" Custom Commands (Might require external dependencies)
command JsonPretty :%!jq .
command JsonMinify :%!jq -c .
" command CopySelection :'<,'>:w !pbcopy
