" Load custom config files
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/colors.vim
source $HOME/.config/nvim/keybindings.vim
source $HOME/.config/nvim/filespecific.vim

" Language
set langmenu=none

" Editor
syntax on
set noerrorbells
set number relativenumber
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set tabstop=2 softtabstop=2
set shiftwidth=2
set expandtab
set smartindent
set smartcase
set colorcolumn=80
set backspace=indent,eol,start
